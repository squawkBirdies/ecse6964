#ECSE-6964 Lab 03
#Lawrence Fan
#Description: Download geometric data for LA, NYC, London, and Beijing from an AWS server in JSON
#	      Parse data for latitude and longitude coordinates of each city and calculate distances
#	      from LA to NYC, NYC to London, and London to Beijing on a sphere


#Imported libraries
import urllib.request, json, math

#Constants
url_base = "https://s3.amazonaws.com/internetworking-of-things/Lab03/"
radius = 6378.137 #Radius of the Earth in km

def haversineDistance(lat1, long1, lat2, long2):
	#@param: latitude and longitude of 2 locations on Earth (float)
	#@return: Float representing great circle distance between two locations based on Haversine formula,
	#	  assuming Earth to be perfectly spherical with radius equal to radius of earth at equator
	lat1_rad = (lat1*math.pi)/180.0
	long1_rad = (long1*math.pi)/180.0
	lat2_rad = (lat2*math.pi)/180.0
	long2_rad = (long2*math.pi)/180.0
	part1 = math.pow(math.sin((lat2_rad - lat1_rad)/2.0), 2)
	part2 = math.cos(lat1_rad)*math.cos(lat2_rad)*math.pow(math.sin((long2_rad - long1_rad)/2.0), 2)
	distance = 2*radius*math.asin(math.sqrt(part1 + part2))
	return distance

def loadCoordinates(city):
	#@param: name of json file of city data (string)
	#@return: tuple of floats representing latitude and longitude of city center
	url_city = url_base + city
	response = urllib.request.urlopen(url_city)
	CityData = json.loads(response.read().decode())
	City_Lat = float(CityData["results"][0]['geometry']['location']['lat'])
	City_Long = float(CityData["results"][0]['geometry']['location']['lng'])
	return City_Lat, City_Long

LA_Lat, LA_Long = loadCoordinates("losangeles.json")
NY_Lat, NY_Long = loadCoordinates("newyork.json")
LD_Lat, LD_Long = loadCoordinates("london.json")
BJ_Lat, BJ_Long = loadCoordinates("beijing.json")

print("Los Angleles")
print("Latitude: " + str(LA_Lat))
print("Longitude: " + str(LA_Long))
print("\nNew York City")
print("Latitude: " + str(NY_Lat))
print("Longitude: " + str(NY_Long))
print("\nLondon")
print("Latitude: " + str(LD_Lat))
print("Longitude: " + str(LD_Long))
print("\nBeijing")
print("Latitude: " + str(BJ_Lat))
print("Longitude: " + str(BJ_Long))

LA2NY = haversineDistance(LA_Lat, LA_Long, NY_Lat, NY_Long)
print("\nDistance from Los Angeles to New York City: " + str(LA2NY) + " km")
NY2LD = haversineDistance(NY_Lat, NY_Long, LD_Lat, LD_Long)
print("Distance from New York City to London: " + str(NY2LD) + " km")
LD2BJ = haversineDistance(LD_Lat, LD_Long, BJ_Lat, BJ_Long)
print("Distance from London to Beijing: " + str(LD2BJ) + " km")
